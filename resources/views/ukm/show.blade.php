@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
<div class="row">
    <div class="col-4">
    <div class="card">
        <img src="{{ asset('images/' . $ukm->foto_produk) }}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">{{ $ukm->nama_produk }}</h5>
          <p class="card-text">{!! $ukm->deskripsi_produk !!}</p>
        </div>
        </div>
      </div>
    </div>
</div>

<div class="container">
<hr>
<h4>List Ulasan</h4>
@forelse ($ukm->ulasan as $item)
<div class="media my-3">
  <img src="{{ asset('images/' . ($detailProfile?->foto_profil ?? 'avatar.png')) }}" class="mr-3" styele="border-radius: 50%;" width="200px" alt="...">
  <div class="media-body">
    <h5 class="mt-0">{{ $item->user->name }}</h5>
    <h5 class="mt-0">Mendapat Rating {{ $item->rating }}</h5>
    <p>{{ $item->komentar }}</p>
  </div>
</div>
@empty
    <h4> Tidak ada Ulasan</h4>
@endforelse
<hr>
<form action="/ulasan/{{ $ukm->id }}" method="POST">
  @csrf
  <div class="form-group mt-4 col-4">
    <label>Rating</label>
    <input type="text" class="form-control" id="rating" name="rating">
    @error('rating')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
@enderror
  </div>
  <div class="form-group mt-4">
    <label>Komentar</label>
    <textarea class="form-control" name="komentar" id="komentar"></textarea> 
    @error('komentar')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
@enderror
  </div>
<input type="submit" value="ulasan" class="mt-2">
</form>
</div>
</div>
<hr>
<a href="/ukm" class="btn btn-secondary mt-2 btn-block float-right">Kembali</a>
@endsection