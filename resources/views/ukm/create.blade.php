@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
    <form action="/ukm" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="nama_pemilik">Nama Pemilik</label>
          <input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik" value="{{ old('nama_pemilik') }}">
          @error('nama_pemilik')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group mt-4">
          <label for="nik">Nomor Induk Kependudukan</label>
          <input type="text" class="form-control" id="nik" name="nik" value="{{ old('nik') }}">
          @error('nik')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group mt-4">
            <label for="alamat">Alamat</label>
            <textarea class="form-control" name="alamat" id="alamat">{{ old('alamat') }}</textarea> 
            @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label for="nama_produk">Nama Produk</label>
            <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="{{ old('nama_produk') }}">
            @error('nama_produk')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label for="deskripsi_produk">Deskripsi Produk</label>
            <textarea class="form-control" name="deskripsi_produk" id="deskripsi_produk">{{ old('deskripsi_produk') }}</textarea> 
            @error('deskripsi_produk')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label for="nomor_telepon">Nomor Telepon</label>
            <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon" value="{{ old('nomor_telepon') }}">
            @error('nomor_telepon')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label>Foto Produk</label>
            <input type="file" class="form-control" id="foto_produk" name="foto_produk" value="{{ old('foto_produk') }}">
            @error('foto_produk')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label>Kategori Produk</label>
           <select id="kategori_id" name="kategori_id" class="form-control">
           <option value="">Pilih Kategori</option>
           @forelse ($kategori as $item)
               <option value="{{ $item->id }}">{{ $item->kategori_produk }}</option>
           @empty
           <option value="">Tidak Ada Kategori Produk</option>
           @endforelse
        </select>
            @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label>Bidang Usaha UKM</label>
           <select id="bidang_id" name="bidang_id" class="form-control">
          
           @forelse ($bidang as $item)
               <option value="{{ $item->id }}">{{ $item->bidang_usaha }}</option>
           @empty
           <option value="">Tidak Ada Bidang Usaha</option>
           @endforelse
        </select>
            @error('bidang_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
        <a href="/ukm" class="btn btn-secondary mt-3">Batal</a>
        <button type="submit" class="btn btn-primary mt-3">Simpan</button>
    </form>
</div>
    
@push('scripts')
<script src="https://cdn.tiny.cloud/1/pf2dywukt2wgyqcw30kc28yte7r1rw5offibrqbmdcn6852r/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      mergetags_list: [
        { value: 'First.Name', title: 'First Name' },
        { value: 'Email', title: 'Email' },
      ]
    });
  </script>
   <script
  src="https://code.jquery.com/jquery-3.6.1.js"
  integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
  crossorigin="anonymous"></script>
   <script src=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

<script>
  $('#kategori_id').select2();
  $('#bidang_id').select2();
</script>
@endpush
@endsection