@extends('layouts.master')
@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
  @if (Session::get('success'))
<div class="alert alert-success">{{ Session::get('success') }}</div>
@endif
  <a href="/ukm/create" class="btn btn-primary btn-sm my-2">Tambah Data</a>
    <table class="table" id="myTable">
        <thead class="thead-light">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Produk</th>
            <th scope="col">Nama Pemilik</th>
            <th scope="col">Nomor Telepon</th>
            <th scope="col">Foto Produk</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            @forelse ($ukm as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
              <td>{{$value->nama_produk}}</td>
              <td>{{$value->nama_pemilik}}</td>
              <td>{{$value->nomor_telepon}}</td>
              <td><img src="{{ asset('images/' . $value->foto_produk) }}" width="200px"></td>
              <td>
                <a href="/ukm/{{ $value->id }}" class="btn btn-success btn-sm my-2">Detail</a>
                <form onsubmit="return confirm('Yakin akan menghapus data ini?')" class="d-inline" action="/ukm/{{ $value->id }}" method="POST">
                  @method('delete')
                  @csrf
                  <a href="/ukm/{{ $value->id }}/edit" class="btn btn-warning btn-sm my-2">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
          </tr>
      @empty
          <tr colspan="3">
              <td>No data</td>
          </tr>  
      @endforelse         
          </tr>
        </tbody>
      </table>
</div>
@push('scripts')

    <script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready( function () {
    $('#myTable').DataTable();
    } );
    </script>
@endpush
@endsection
