@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
@if (Session::get('success'))
<div class="alert alert-success">{{ Session::get('success') }}</div>
@endif
<div class="container">
    <a href="/kategori/create" class="btn btn-primary btn-sm my-2">Tambah Data</a>
    <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Kategori Produk</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            @forelse ($kategori as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
              <td>{{$value->kategori_produk}}</td>
              <td>
                <a href="/kategori/{{ $value->id }}" class="btn btn-success btn-sm my-2">Detail</a>
                <form onsubmit="return confirm('Yakin akan menghapus data ini?')" class="d-inline" action="/kategori/{{ $value->id }}" method="POST">
                  @method('delete')
                  @csrf
                  <a href="/kategori/{{ $value->id }}/edit" class="btn btn-warning btn-sm my-2">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
          </tr>
      @empty
          <tr colspan="3">
              <td>No data</td>
          </tr>  
      @endforelse         
          </tr>
        </tbody>
      </table>
</div>
    
@endsection