@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
<form action="/kategori/{{ $kategori->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group col-8">
      <label for="kategori_produk">Kategori Produk UKM</label>
      <input type="text" class="form-control" id="kategori_produk" name="kategori_produk" value="{{ $kategori->kategori_produk }}">
      @error('kategori_produk')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <a href="/kategori" class="btn btn-secondary mt-2">Batal</a>
    <button type="submit" class="btn btn-primary mt-2">Simpan</button>
</form>
</div>
@endsection