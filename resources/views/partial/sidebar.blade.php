<!-- ======= Header ======= -->
<header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('images/' . ($detailProfile?->foto_profil ?? 'avatar.png')) }}" alt="" class="img-fluid rounded-circle">
        @auth 
        <h1 class="text-light">{{ Auth::user()->name }}</h1>
        @endauth

        @guest
        <h1 class="text-light">Anda Belum Login</h1>
        @endguest
      </div>  
      @auth
      <nav id="navbar" class="nav-menu navbar">
        <ul>
          <li><a href="/" class="nav-link scrollto"><i class="bx bx-home"></i> <span>Home</span></a></li>
          <li><a href="/profile" class="nav-link scrollto"><i class="bx bx-user"></i> <span>My Profile</span></a></li>
          <li><a href="/ukm" class="nav-link scrollto"><i class="bx bx-book-content"></i> <span>Data Ukm</span></a></li>
          <li><a href="/kategori" class="nav-link scrollto"><i class="bx bx-file-blank"></i> <span>Kategori</span></a></li>
          <li><a href="/bidang" class="nav-link scrollto"><i class="bx bx-text"></i> <span>Bidang Usaha</span></a></li>
          {{-- <li><a href="#services" class="nav-link scrollto"><i class="bx bx-server"></i> <span>Services</span></a></li>
          <li><a href="#contact" class="nav-link scrollto"><i class="bx bx-envelope"></i> <span>Contact</span></a></li> --}}
        </ul>
      </nav><!-- .nav-menu -->
    </div>
    @endauth
  </header><!-- End Header -->