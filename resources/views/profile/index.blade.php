@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
    <form action="/profile/{{ $detailProfile->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group mt-4">
          <label>Nama Lengkap</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ $detailProfile->user->name }}" disabled>
          @error('name')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group mt-4">
          <label>Email</label>
          <input type="email" class="form-control" id="email" name="email" value="{{ $detailProfile->user->email }}" disabled>
          @error('email')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group mt-4">
          <label>Umur</label>
          <input type="number" class="form-control" id="umur" name="umur" value="{{ $detailProfile->umur }}">
          @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group mt-4">
          <label>Nomor Telepon</label>
          <input type="number" class="form-control" id="nomor_telepon" name="nomor_telepon" value="{{ $detailProfile->nomor_telepon }}">
          @error('nomor_telepon')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group mt-4">
            <label>Biodata</label>
            <textarea class="form-control" name="bio" id="bio">{{  $detailProfile->bio  }}</textarea> 
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" id="alamat">{{  $detailProfile->alamat  }}</textarea> 
            @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
          <div class="form-group mt-4">
            <label>Foto profil</label>
            <input type="file" class="form-control" id="foto_profil" name="foto_profil">
            @error('foto_profil')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
          </div>
        <a href="/profile" class="btn btn-secondary mt-3">Batal</a>
        <button type="submit" class="btn btn-primary mt-3">Simpan</button>
    </form>
</div>
@endsection