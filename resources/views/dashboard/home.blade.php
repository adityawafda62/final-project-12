 @extends('layouts.master')

 @section('title')
 <h3> Halo selamat datang </h3>
 @endsection

 @section('content')
  
      <div class="container">

        <div class="section-title">
          <h2>Tentang Sistem Yang Kami Buat</h2>
          <p>Sistem Ini di buat untuk memenuhi tugas Akhir Kelompok 12 Bootcamp Sanbercode 39</p>
        </div>

        <div class="row">
          {{-- <div class="col-lg-4" data-aos="fade-right">
            <img src="{{ asset ('images/Final Project.png') }}" class="img-fluid" alt="">
          </div> --}}
          <div class="col-lg-12 pt-4 pt-lg-0 content" data-aos="fade-left">
            <h3>Tema Sistem Informasi UKM Sederhana Berbasis Website</h3>
            <p class="fst-italic">
            Spesifikasi Website yang kami Buat :
            </p>
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>HTML :</strong> <span>HTML 5</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Framework CSS :</strong> <span>Boostrap 4</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Framework PHP :</strong> <span>Laravel 8 </span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Database : </strong> <span>Mysql</span></li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>CRUD : </strong> <span>Memilik 3 CRUD, Data UKM, Kategori, Bidang</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Eloquent Relationships : </strong> <span>One To One, One To Many, Many To Many</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Library :</strong> <span>Sweet Alert, TinyMCE, dan Select2</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Auth : </strong> <span>Register dan Login</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
      <section id="testimonials" class="testimonials section-bg">
        <div class="container">
  
          <div class="section-title">
            <h2>Profile Kelompok 12</h2>
            <p>Berikut Anggota Profile Kelompok 12</p>
          </div>
  
          <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
            <div class="swiper-wrapper">
  
              <div class="swiper-slide">
                <div class="testimonial-item" data-aos="fade-up">
                  <img src="{{ asset ('images/Iqbal.png') }}" class="testimonial-img" alt="">
                  <h3>Syaiful Iqbal</h3>
                  <h4>Anggota Kelompok 12</h4>
                </div>
              </div><!-- End testimonial item -->
  
              <div class="swiper-slide">
                <div class="testimonial-item" data-aos="fade-up" data-aos-delay="100">
                 
                  <img src="{{ asset ('images/Trisna.jpeg') }}" class="testimonial-img" alt="">
                  <h3>Trisna Jaya</h3>
                  <h4>Anggota Kelompok 12</h4>
                </div>
              </div><!-- End testimonial item -->
  
              <div class="swiper-slide">
                <div class="testimonial-item" data-aos="fade-up" data-aos-delay="200">
                 
                  <img src="{{ asset ('images/Avatar.png') }}" class="testimonial-img" alt="">
                  <h3>Aditya Wafda Nahari</h3>
                  <h4>Anggota Kelompok 12</h4>
                </div>
              </div><!-- End testimonial item -->
            </div>
          </div>
  
        </div>
      </section><!-- End Testimonials Section -->
    
 @endsection