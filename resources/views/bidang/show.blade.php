@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
    <h1 class="text-primary">{{ $bidang->bidang_usaha }}</h1>
<div class="row">
    @forelse ($bidang->ukm as $item)
        
            <div class="container">
                <div class="row">
                    <div class="col-4">
                    <div class="card">
                        <img src="{{ asset('images/' . $item->foto_produk) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">{{ $item->nama_produk }}</h5>
                          <p class="card-text">{!! $item->deskripsi_produk !!}</p>
                          <a href="/bidang" class="btn btn-secondary">Kembali</a>
                        </div>
                    </div>
                </div>
            
            @empty
            <h3> Kategori ini tidak ada postingan</h3>
            @endforelse
        </div>
       
</div>
@endsection