@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
    <a href="/bidang/create" class="btn btn-primary btn-sm my-2">Tambah Data</a>
    <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Bidang Usaha</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            @forelse ($bidang as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
              <td>{{$value->bidang_usaha}}</td>
              <td>
                <a href="/bidang/{{ $value->id }}" class="btn btn-success btn-sm my-2">Detail</a>
                <form onsubmit="return confirm('Yakin akan menghapus data ini?')" class="d-inline" action="/bidang/{{ $value->id }}" method="POST">
                  @method('delete')
                  @csrf
                  <a href="/bidang/{{ $value->id }}/edit" class="btn btn-warning btn-sm my-2">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
          </tr>
      @empty
          <tr colspan="3">
              <td>No data</td>
          </tr>  
      @endforelse         
          </tr>
        </tbody>
      </table>
</div>
    
@endsection