@extends('layouts.master')

@section('title')
    <h3>{{ $title }}</h3>
@endsection

@section('content')
<div class="container">
<form action="/bidang" method="POST">
    @csrf
    <div class="form-group col-8">
      <label for="bidang_usaha">Bidang Usaha UKM</label>
      <input type="text" class="form-control" id="bidang_usaha" name="bidang_usaha" value="{{ old('bidang_usaha') }}">
      @error('bidang_usaha')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <a href="/bidang" class="btn btn-secondary mt-2">Batal</a>
    <button type="submit" class="btn btn-primary mt-2">Simpan</button>
</form>
</div>
    
@endsection