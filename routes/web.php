<?php

use App\Http\Controllers\BidangController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UkmController;
use App\Http\Controllers\UlasanController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::middleware(['auth'])->group(function () {
    // CRUD Table Kategori
    // Crud Film
    Route::resource('kategori', KategoriController::class);
    // CRUD Table UKM
    // Crud Film
    Route::resource('ukm', UkmController::class);
    Route::resource('bidang', BidangController::class);
    Route::resource('profile', ProfileController::class)->only(['index', 'update']);
    // Ulasan
    Route::post('/ulasan/{ukm_id}', [UlasanController::class, 'tambah']);
});
Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
