## Final Project

## Kelompok 12

1. Aditya Wafda Nahari
2. Syaiful Iqbal
3. Trisna Jaya

## Tema Project

Sistem Informasi UKM

## ERD

<img src="public/images/Final Project.png" alt="Total Downloads"></a>

## Link Video

Link Deploy :
http://kelompok-12laravel.sanbercodeapp.com/

Link Demo :
https://youtu.be/Dm3flSjR3YU