<?php

namespace App\Http\Controllers;

use App\Models\BidangModel;
use App\Models\UkmModel;
use App\Models\KategoriModel;
use App\Models\ProfileModel;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class UkmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ukm = UkmModel::all();
        $detailProfile = ProfileModel::where('user_id')->first();
        return view('ukm.ukm', [
            'detailProfile' => $detailProfile,
            'iduser' => 'detailProfile',
            'ukm' => $ukm,
            'title' => 'Halaman Data Ukm',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = KategoriModel::all();
        $detailProfile = ProfileModel::where('user_id')->first();
        $bidang = BidangModel::all();
        return view('ukm.create', [
            'kategori' => $kategori,
            'detailProfile' => $detailProfile,
            'bidang' => $bidang,
            'title' => 'Halaman Tambah Data Ukm',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'nama_pemilik' => 'required',
                'nik' => 'required|numeric',
                'alamat' => 'required',
                'nama_produk' => 'required',
                'deskripsi_produk' => 'required',
                'nomor_telepon' => 'required|numeric',
                'foto_produk' => 'required|mimes:jpg,jpeg,png',
                'kategori_id' => 'required',
                'bidang_id' => 'required',
            ],
            [
                'nama_pemilik.required' => 'Nama Pemilik Wajib diisi',
                'nik.required' => 'Nomor Nik Wajib diisi',
                'nik.numeric' => 'Nomor Nik hanya bisa diisi dengan angka',
                'alamat.required' => 'Alamat Wajib diisi',
                'nama_produk.required' => 'Nama Produk wajib diisi',
                'deskripsi_produk.required' => 'Deskripsi Produk Wajib diisi',
                'nomor_telepon.required' => 'Nomor Telepon Wajib diisi',
                'nomor_telepon.numeric' => 'Nomor Telepon hanya bisa diisi dengan angka',
                'foto_produk.required' => 'Foto Produk belum dipilih',
                'kategori_id.required' => 'Kategori UKM belum dipilih',
                'bidang_id.required' => 'Bidang Usaha belum dipilih',
            ]
        );

        $imageName = time() . '.' . $request->foto_produk->extension();
        $request->foto_produk->move(public_path('images'), $imageName);

        UkmModel::create([
            'nama_pemilik' => $request['nama_pemilik'],
            'nik' => $request['nik'],
            'alamat' => $request['alamat'],
            'nama_produk' => $request['nama_produk'],
            'deskripsi_produk' => $request['deskripsi_produk'],
            'nomor_telepon' => $request['nomor_telepon'],
            'foto_produk' => $imageName,
            'kategori_id' => $request['kategori_id'],
            'bidang_id' => $request['bidang_id']
        ]);
        Alert::success('Success', 'Berhasil tambah Data');
        return redirect('ukm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ukm = UkmModel::find($id);
        $detailProfile = ProfileModel::where('user_id')->first();
        return view('ukm.show', compact('ukm'), [
            'detailProfile' => $detailProfile,
            'title' => 'Halaman Detail UKM',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ukm = UkmModel::find($id);
        $kategori = KategoriModel::all();
        $bidang = BidangModel::all();
        $detailProfile = ProfileModel::where('user_id')->first();
        return view('ukm.edit', [
            'ukm' => $ukm,
            'kategori' => $kategori,
            'bidang' => $bidang,
            'detailProfile' => $detailProfile,
            'title' => 'Halaman Edit Data Ukm',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'nama_pemilik' => 'required',
                'nik' => 'required|numeric',
                'alamat' => 'required',
                'nama_produk' => 'required',
                'deskripsi_produk' => 'required',
                'nomor_telepon' => 'required|numeric',
                'foto_produk' => 'required|mimes:jpg,jpeg,png',
                'kategori_id' => 'required',
                'bidang_id' => 'required',
            ],
            [
                'nama_pemilik.required' => 'Nama Pemilik Wajib diisi',
                'nik.required' => 'Nomor Nik Wajib diisi',
                'nik.numeric' => 'Nomor Nik hanya bisa diisi dengan angka',
                'alamat.required' => 'Alamat Wajib diisi',
                'nama_produk.required' => 'Nama Produk wajib diisi',
                'deskripsi_produk.required' => 'Deskripsi Produk Wajib diisi',
                'nomor_telepon.required' => 'Nomor Telepon Wajib diisi',
                'nomor_telepon.numeric' => 'Nomor Telepon hanya bisa diisi dengan angka',
                'foto_produk.required' => 'Foto Produk belum dipilih',
                'kategori_id.required' => 'Kategori UKM belum dipilih',
                'bidang_id.required' => 'Bidang Usaha belum dipilih'
            ]
        );

        $ukm = UkmModel::find($id);
        if ($request->has('foto_produk')) {
            $path = 'images/';
            File::delete($path . $ukm->foto_produk);

            $imageName = time() . '.' . $request->foto_produk->extension();
            $request->foto_produk->move(public_path('images'), $imageName);

            $ukm->foto_produk = $imageName;
            $ukm->save();

            $ukm->nama_pemilik = $request->nama_pemilik;
            $ukm->nik = $request->nik;
            $ukm->alamat = $request->alamat;
            $ukm->nama_produk = $request->nama_produk;
            $ukm->deskripsi_produk = $request->deskripsi_produk;
            $ukm->nomor_telepon = $request->nomor_telepon;
            $ukm->kategori_id = $request->kategori_id;
            $ukm->bidang_id = $request->bidang_id;
            $ukm->save();
            Alert::success('Success', 'Berhasil Ubah Data');

            return redirect('ukm');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ukm = UkmModel::find($id);
        $ukm->delete();
        Alert::success('Success', 'Berhasil Hapus Data');
        return redirect('ukm');
    }
}
