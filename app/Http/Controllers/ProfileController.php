<?php

namespace App\Http\Controllers;

use App\Models\ProfileModel;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function index()
    {
        $iduser = Auth::id();
        $detailProfile = ProfileModel::where('user_id', $iduser)->first();
        return view('profile.index', [
            'detailProfile' => $detailProfile,
            'title' => 'Halaman Profile',
        ]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required',
            'nomor_telepon' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
            'foto_profil' => 'mimes:jpg,jpeg,png'
        ]);

        $profile = ProfileModel::find($id);
        if ($request->has('foto_profil')) {
            $path = 'images/';
            if ($profile->foto_profil != null) {
                File::delete($path . $profile->foto_profil);
            }
            $imageName = time() . '.' . $request->foto_profil->getClientOriginalName();
            $request->foto_profil->move(public_path('images'), $imageName);
            $profile->foto_profil = $imageName;
            $profile->save();
        }
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->bio = $request->bio;
        $profile->nomor_telepon = $request->nomor_telepon;
        $profile->save();
        Alert::success('Success', 'Berhasil Ubah');
        return redirect('profile');
    }
}
