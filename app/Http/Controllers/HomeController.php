<?php

namespace App\Http\Controllers;

use App\Models\ProfileModel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function utama()
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        return view(
            'dashboard.home',
            [
                'title' => 'Halaman Dashboard',
                'detailProfile' => $detailProfile
            ]
        );
    }
}
