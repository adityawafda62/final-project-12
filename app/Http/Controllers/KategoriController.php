<?php

namespace App\Http\Controllers;

use App\Models\KategoriModel;
use App\Models\ProfileModel;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Session;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = KategoriModel::all();
        $detailProfile = ProfileModel::where('user_id')->first();
        return view('kategori.kategori', [
            'detailProfile' => $detailProfile,
            'kategori' => $kategori,
            'title' => 'Halaman Data kategori Ukm',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        $kategori = KategoriModel::all();
        return view('kategori.create', [
            'kategori' => $kategori,
            'detailProfile' => $detailProfile,
            'title' => 'Halaman Tambah Data Ukm',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'kategori_produk' => 'required',

            ],
            [
                'kategori_produk.required' => 'kategori produk Wajib diisi',
            ]
        );

        KategoriModel::create([
            'kategori_produk' => $request['kategori_produk']
        ]);
        Alert::success('Success', 'Berhasil Tambah Data');
        return redirect('kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        $kategori = KategoriModel::find($id);
        return view('kategori.show', compact('kategori'), [
            'detailProfile' => $detailProfile,
            'title' => 'Halaman Detail Kategori UKM',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        $kategori = KategoriModel::find($id);
        return view('kategori.edit', [
            'kategori' => $kategori,
            'detailProfile' => $detailProfile,
            'title' => 'Halaman Edit Data kategori Ukm',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'kategori_produk' => 'required',

            ],
            [
                'kategori_produk.required' => 'kategori produk Wajib diisi',
            ]
        );
        $kategori = kategoriModel::find($id);
        $kategori->kategori_produk = $request->kategori_produk;
        $kategori->update();
        Alert::success('Success', 'Berhasil Ubah Data');
        return redirect('kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = KategoriModel::find($id);
        $kategori->delete();
        Alert::success('Success', 'Berhasil Hapus Data');
        return redirect('kategori');
    }
}
