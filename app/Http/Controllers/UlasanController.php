<?php

namespace App\Http\Controllers;

use App\Models\UlasanModel;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class UlasanController extends Controller
{
    public function tambah(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'rating' => 'required',
                'komentar' => 'required',

            ],
            [
                'rating.required' => 'Form ini Wajib diisi',
                'komentar.required' => 'Form ini Wajib diisi',
            ]
        );
        $iduser = Auth::id();

        $ulasan = new UlasanModel();

        $ulasan->rating = $request->rating;
        $ulasan->komentar = $request->komentar;
        $ulasan->user_id = $iduser;
        $ulasan->ukm_id = $id;

        $ulasan->save();
        Alert::success('Success', 'Berhasil Tambah Data');
        return redirect('/ukm/' . $id);
    }
}
