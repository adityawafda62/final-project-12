<?php

namespace App\Http\Controllers;

use App\Models\BidangModel;
use App\Models\ProfileModel;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;

class BidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        $bidang = BidangModel::all();
        return view('bidang.bidang', [
            'detailProfile' => $detailProfile,
            'bidang' => $bidang,
            'title' => 'Halaman Data Bidang Ukm',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        $bidang = BidangModel::all();
        return view('bidang.create', [
            'detailProfile' => $detailProfile,
            'bidang' => $bidang,
            'title' => 'Halaman Tambah Data Bidang Ukm',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'bidang_usaha' => 'required',

            ],
            [
                'bidang_usaha.required' => 'form ini Wajib diisi',
            ]
        );

        BidangModel::create([
            'bidang_usaha' => $request['bidang_usaha']
        ]);
        Alert::success('Success', 'Berhasil Tambah Data');
        return redirect('bidang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        $bidang = BidangModel::find($id);
        return view('bidang.show', compact('bidang'), [
            'detailProfile' => $detailProfile,
            'title' => 'Halaman Detail Bidang UKM',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detailProfile = ProfileModel::where('user_id')->first();
        $bidang = BidangModel::find($id);
        return view('bidang.edit', [
            'detailProfile' => $detailProfile,
            'bidang' => $bidang,
            'title' => 'Halaman Edit Data Bidang Ukm',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'bidang_usaha' => 'required',

            ],
            [
                'bidang_usaha.required' => 'Form ini Wajib diisi',
            ]
        );
        $bidang = BidangModel::find($id);
        $bidang->bidang_usaha = $request->bidang_usaha;
        $bidang->update();
        Alert::success('Success', 'Berhasil Ubah Data');
        return redirect('bidang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bidang = BidangModel::find($id);
        $bidang->delete();
        Alert::success('Success', 'Berhasil Hapus Data');
        return redirect('bidang');
    }
}
