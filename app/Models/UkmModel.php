<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UkmModel extends Model
{
    use HasFactory;
    protected $table = "ukm";
    protected $fillable = ["nama_pemilik", "nik", "alamat", "nama_produk", "deskripsi_produk", "nomor_telepon", "foto_produk", "kategori_id", "bidang_id"];

    public function kategori()
    {
        return $this->belongsTo(KategoriModel::class, 'kategori_id');
    }
    public function bidang()
    {
        return $this->belongsTo(BidangModel::class, 'bidang_id');
    }
    public function ulasan()
    {
        return $this->hasMany(UlasanModel::class, 'ukm_id');
    }
}
