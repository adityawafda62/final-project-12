<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UlasanModel extends Model
{
    use HasFactory;
    protected $table = "ulasan";
    protected $fillable = ["rating", "komentar", "ukm_id", "user_id"];

    public function ukm()
    {
        return $this->belongsTo(UkmModel::class, 'ukm_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
