<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BidangModel extends Model
{
    use HasFactory;
    protected $table = "bidang";
    protected $fillable = ["bidang_usaha"];

    public function ukm()
    {
        return $this->hasMany(UkmModel::class, 'bidang_id');
    }
}
