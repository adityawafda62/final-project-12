<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    use HasFactory;
    protected $table = "kategori";
    protected $fillable = ["kategori_produk"];
    public function ukm()
    {
        return $this->hasMany(UkmModel::class, 'kategori_id');
    }
}
